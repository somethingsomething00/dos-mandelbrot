# Mandelbrot DOS

```
Usage: mandel.exe [ITERATIONS]

Renders the mandelbrot set in VGA 320x200 mode
Accepts an optional first argument to set the iteration count

Building:
build.bat

Running:
mandel.exe

Controls:
Escape or Q to quit during rendering
Escape, Q, or Enter to quit after rendering


Originally compiled with Borland C++ 3.1

Keyboard driver reference:
Black Art of 3D Game Programming
Andre LaMothe
```

![thumbnail](./thumbnail.png)

* Taken with `mandel.exe 16`
